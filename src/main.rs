#![allow(non_snake_case)]
// Libreria para parsear argumentos
use structopt::StructOpt;

// Usa la libreria
#[derive(StructOpt)]
struct Argumentos {
    patron: String,
    #[structopt(parse(from_os_str))]
    path: std::path::PathBuf, 
}

fn main()  -> Result<(), Box<dyn std::error::Error>> {
    let args = Argumentos::from_args();
    // Abre el archivo
    let file = std::fs::read_to_string(&args.path)?;
    // PARTE PARALELIZABLE
    // Recorre el archivo y busca coincidencias
    for line in file.lines() {
        if line.contains(&args.patron) {
            println!("{}", line);
        }
    }
    Ok(());
}